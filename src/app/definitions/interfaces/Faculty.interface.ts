export interface IFaculty {
    id: string,
    university: string,
    name: string,
    staffsCount: number,
    professors: number,
    associateProfessors: number,
    teachers: number,
    articles: number,
    dissertations: number,
    rating: number,
    image: string
}
