export interface IStaff {
    id: string,
    name: string,
    title: string,
    avatar: string,
    content: string,
    articles: number,
    dissertation: number,
    thesis: number,
    rank: string
}
