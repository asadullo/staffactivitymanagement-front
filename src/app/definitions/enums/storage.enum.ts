export enum STORAGE  {
    USER = 'user',
    THEME = 'theme',
    LANG = 'lang',
    SESSION = 'session'
}
