import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import {LANGUAGE, STORAGE, THEME} from "../../definitions";

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  lang: string = LANGUAGE.EN;
  hide: boolean = false;
  form!: FormGroup;
  loading!: boolean;

  constructor(private router: Router, private fb: FormBuilder) {
    this.loading = false;
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required, Validators.minLength(3)]
    })
  }

  ngOnInit(): void {
  }

  onLogin(loginForm: FormGroup) {
    if(loginForm.invalid) {
      return false;
    }

    console.log(loginForm);

    if(loginForm.value.email == 'asadulla@payme.uz', loginForm.value.password == 'Asadullo2807') {
      const session: string = 'jwtString';
      const user = {
        name: "Asadullo",
        role: 'super user'
      }
      localStorage.setItem(STORAGE.SESSION, session);
      localStorage.setItem(STORAGE.LANG , LANGUAGE.UZ);
      localStorage.setItem(STORAGE.THEME, THEME.LIGHT);
      localStorage.setItem(STORAGE.USER, JSON.stringify(user));
      this.router.navigate['/pages/dashboard'];
    }

    console.log('Login form works');

  }

}
