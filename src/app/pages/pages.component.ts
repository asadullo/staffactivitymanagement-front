import {Component, OnInit} from '@angular/core';

import { MENU_ITEMS } from './pages-menu';
import { TranslateService } from "@ngx-translate/core";
import { NbMenuItem } from "@nebular/theme";

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="sidebarMenu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent implements OnInit {

  constructor(private _translate: TranslateService) {
    this._translate.use('en');
  }

  ngOnInit(): void {
    console.log('Menu: ', this.sidebarMenu);
    console.log(this._translate.getLangs());
  }
  menu = MENU_ITEMS;

  translate(key: string): string {
    return this._translate.instant(key)
  }

  sidebarMenu: NbMenuItem[] = [
    {
      title: 'Dashboard',
      icon: { icon: 'dashboard', pack: 'fas' },
      link: '/pages/dashboard',
      home: true,
    },
    // {
    //   title: 'IoT Dashboard',
    //   icon: 'home-outline',
    //   link: '/pages/iot-dashboard',
    // },
    {
      title: 'FEATURES',
      group: true,
    },
    {
      title: 'Staffs',
      icon: 'people',
      link: '/pages/staff',
    },
    {
      title: 'Scientific works',
      icon: { icon: 'atom', pack: 'fas' },
      link: '/pages/staff/scientific-works',
    },
    {
      title: 'Faculties',
      icon: { icon: 'building-columns', pack: 'fas' },
      link: '/pages/faculty',
    },
    {
      title: 'Profile',
      icon: { icon: 'user-circle', pack: 'fas' },
      expanded: true,
      children: [
        {
          title: 'Change Password',
        },
        {
          title: 'Privacy Policy',
        },
        {
          title: 'Logout',
        },
      ],
    },
  ]
}
