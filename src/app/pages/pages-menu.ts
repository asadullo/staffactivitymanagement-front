import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: { icon: 'dashboard', pack: 'fas' },
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'FEATURES',
    group: true,
  },
  {
    title: 'Staffs',
    icon: 'people',
    link: '/pages/staff',
  },
  {
    title: 'Scientific works',
    icon: { icon: 'atom', pack: 'fas' },
    link: '/pages/staff/scientific-works',
  },
  {
    title: 'Faculties',
    icon: { icon: 'building-columns', pack: 'fas' },
    link: '/pages/faculty',
  },
  {
    title: 'Profile',
    icon: { icon: 'user-circle', pack: 'fas' },
    expanded: true,
    children: [
      {
        title: 'Change Password',
      },
      {
        title: 'Privacy Policy',
      },
      {
        title: 'Logout',
      },
    ],
  },
];
