import {Component, OnInit, ViewChild} from '@angular/core';
import {FacultyService} from "./faculty.service";
import {IFaculty} from "../../definitions";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'ngx-faculty',
  templateUrl: './faculty.component.html',
  styleUrls: ['./faculty.component.scss']
})
export class FacultyComponent implements OnInit {
  faculties: IFaculty[];
  displayedColumns: string[] = ['number', 'image', 'university', 'name', 'staffCount', 'professors', 'rating'];
  dataSource = new MatTableDataSource<IFaculty>();

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchValue: string;

  constructor(private facultyService: FacultyService) {
    this.faculties = facultyService.getFaculties();
    this.dataSource.data = this.faculties;
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter() {
    this.dataSource.filter = this.searchValue.trim().toLowerCase();
  }

  downloadExcel() {
    console.log('Implement download Excel logic here...');
  }

}
