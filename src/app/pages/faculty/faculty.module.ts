import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FacultyRoutingModule } from './faculty-routing.module';
import { FacultyComponent } from './faculty.component';
import {MaterialModule} from "../../material/material.module";
import {FacultyService} from "./faculty.service";


@NgModule({
  declarations: [
    FacultyComponent
  ],
  imports: [
    CommonModule,
    FacultyRoutingModule,
      MaterialModule
  ],
  providers: [ FacultyService ]
})
export class FacultyModule { }
