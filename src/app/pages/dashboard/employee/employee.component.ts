import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {StaffService} from "../../staff/staff.service";
import {IStaff} from "../../../definitions";
import * as HighCharts from 'highcharts';

@Component({
  selector: 'ngx-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements AfterViewInit {
  staff: IStaff;

  lastArticles = [
    { title: 'Информационные технологии', citedBy: 10, year: 2022, authors: 'ВФ Гузик, АИ Костюк, ЕВ Ляпунцова, БВ Катаев, МЮ Поленов' },
    { title: 'To properties of solutions to reaction-diﬀusion equation with double nonlinearity with distributed parameters', citedBy: 5, year: 2021, authors: 'A Mersaid' },
    { title: 'Asymptotics of the solutions of the non-Newton politropic filtration equation', citedBy: 8, year: 2020, authors: 'A Mersaid' },
    { title: 'Qualitative properties of solutions of a doubly nonlinear reaction-diffusion system with a source', citedBy: 8, year: 2020, authors: 'A Mersaid, A Sadullayev' },
    { title: 'Ontology of grammar rules as example of noun of Uzbek and Kazakh languages', citedBy: 8, year: 2020, authors: 'M Aripov, A Sharipbay, N Abdurakhmonova, B Razakhova' },
    // Add more articles as needed
  ];

  worksData = [
    { year: '2018', count: 65 },
    { year: '2019', count: 89 },
    { year: '2020', count: 110 },
    { year: '2021', count: 102 },
    { year: '2022', count: 107 },
    { year: '2023', count: 35  },
    // Add more data as needed
  ];

  @ViewChild('barGraphContainer', { static: false }) barGraphContainer: ElementRef;
  @ViewChild('doughnutChartContainer', { static: false }) doughnutChartContainer: ElementRef;
  constructor(private staffService: StaffService) {
    this.staff = staffService.findStaffById('647da64e1c9bb2c55693a922');
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.createBarChart();
    this.createDoughnutChart();
  }
  createBarChart() {
    const chartOptions: HighCharts.Options = {
      chart: {
        type: 'column',
        backgroundColor: '#9cdbe3',
        width: 300,
        height: 300,
      },
      title: {
        text: 'Scientific Works Count Across Years'
      },
      xAxis: {
        categories: this.worksData.map(data => data.year),
      },
      yAxis: {
        title: {
          text: 'Count'
        }
      },
      series: [{
        name: 'Scientific works',
        data: this.worksData.map(data => data.count)
      }as any]
    };

    HighCharts.chart(this.barGraphContainer.nativeElement, chartOptions);
  }

  createDoughnutChart() {
    const doughnutChartOptions: Highcharts.Options = {
      chart: {
        type: 'pie',
        width: 300,
        height: 300,
        backgroundColor: '#9cdbe3'
      },
      title: {
        text: 'Scientific Works Distribution',
      },
      plotOptions: {
        pie: {
          innerSize: '50%', // Specify the inner size of the doughnut chart
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.y}',
          },
        },
      },
      series: [
        {
          name: 'Scientific Works',
          colorByPoint: true,
          data: [
            {
              name: 'Thesis',
              y: 10,
            },
            {
              name: 'Articles',
              y: 20,
            },
            {
              name: 'Dissertations',
              y: 15,
            },
            {
              name: 'Others',
              y: 5,
            },
          ],
        } as any,
      ],
    };

    HighCharts.chart(this.doughnutChartContainer.nativeElement, doughnutChartOptions);
  }


}
