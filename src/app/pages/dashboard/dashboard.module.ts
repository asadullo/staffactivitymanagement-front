import { NgModule } from '@angular/core';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbTabsetModule,
  NbUserModule,
  NbRadioModule,
  NbSelectModule,
  NbListModule,
  NbIconModule,
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { FormsModule } from '@angular/forms';
import {MaterialModule} from "../../material/material.module";
import {DashboardRoutingModule} from "./dashboard-routing.module";
import { ChartModule } from 'angular-highcharts';
import { EmployeeComponent } from './employee/employee.component';
import {StaffModule} from "../staff/staff.module";

@NgModule({
  imports: [
    FormsModule,
    ThemeModule,
    NbCardModule,
    NbUserModule,
    NbButtonModule,
    NbTabsetModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbListModule,
    NbIconModule,
    NbButtonModule,
    MaterialModule,
    DashboardRoutingModule,
      ChartModule,
      StaffModule
  ],
  declarations: [
    DashboardComponent,
    EmployeeComponent,
  ],
})
export class DashboardModule { }
