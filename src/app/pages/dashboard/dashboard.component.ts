import {AfterViewInit, Component, ElementRef, OnDestroy, ViewChild} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators' ;
import { SolarData } from '../../@core/data/solar';
import * as HighCharts from 'highcharts';
interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements AfterViewInit {

  private alive = true;

  solarValue: number;
  lightCard: CardSettings = {
    title: 'Light',
    iconClass: 'nb-lightbulb',
    type: 'primary',
  };
  rollerShadesCard: CardSettings = {
    title: 'Roller Shades',
    iconClass: 'nb-roller-shades',
    type: 'success',
  };
  wirelessAudioCard: CardSettings = {
    title: 'Wireless Audio',
    iconClass: 'nb-audio',
    type: 'info',
  };
  coffeeMakerCard: CardSettings = {
    title: 'Coffee Maker',
    iconClass: 'nb-coffee-maker',
    type: 'warning',
  };

  statusCards: string;

  commonStatusCardsSet: CardSettings[] = [
    this.lightCard,
    this.rollerShadesCard,
    this.wirelessAudioCard,
    this.coffeeMakerCard,
  ];

  statusCardsByThemes: {
    default: CardSettings[];
    cosmic: CardSettings[];
    corporate: CardSettings[];
    dark: CardSettings[];
  } = {
    default: this.commonStatusCardsSet,
    cosmic: this.commonStatusCardsSet,
    corporate: [
      {
        ...this.lightCard,
        type: 'warning',
      },
      {
        ...this.rollerShadesCard,
        type: 'primary',
      },
      {
        ...this.wirelessAudioCard,
        type: 'danger',
      },
      {
        ...this.coffeeMakerCard,
        type: 'info',
      },
    ],
    dark: this.commonStatusCardsSet,
  };

  totalUsersCount: number;
  totalDissertations: number;
  totalArticleCounts: number;
  totalUniversityCounts: number;

  lineGraphData: any[];

  @ViewChild('chartContainer', { static: false }) chartContainer: ElementRef;
  constructor(private themeService: NbThemeService,
              private solarService: SolarData) {
    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.statusCards = this.statusCardsByThemes[theme.name];
    });

    this.solarService.getSolarData()
      .pipe(takeWhile(() => this.alive))
      .subscribe((data) => {
        this.solarValue = data;
      });

    this.totalUsersCount = 1000;
    this.totalDissertations = 500;
    this.totalArticleCounts = 2000;
    this.totalUniversityCounts = 50;

    this.lineGraphData = [
      { month: 'January', count: 100 },
      { month: 'February', count: 150 },
      { month: 'March', count: 200 },
      { month: 'April', count: 250 },
      { month: 'May', count: 300 },
      { month: 'June', count: 350 }
    ];
  }

  ngAfterViewInit() {
    this.createLineChart();
  }

  createLineChart() {
    const chartOptions: HighCharts.Options = {
      chart: {
        type: 'line',
        height: '300px' // Set the desired height of the chart
      },
      title: {
        text: 'Monthly Article Counts'
      },
      xAxis: {
        categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
      },
      yAxis: {
        title: {
          text: 'Count'
        }
      },
      series: [{
        name: 'Articles',
        data: [200, 150, 162, 345, 300, 350, 550, 100, 209, 345, 129, 218]
      }as any]
    };

    HighCharts.chart(this.chartContainer.nativeElement, chartOptions);
  }

  downloadExcel() {
    // @TODO
    console.log('Download analytics to Excel logic should implement here');
  }
}
