import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {DashboardComponent} from "./dashboard.component";
import {EmployeeComponent} from "./employee/employee.component";


const routes: Routes = [
    {
        path: '',
        component: DashboardComponent,
        pathMatch: "full",
        children: [
        ]
    },

    {
        path: 'employee',
        component: EmployeeComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class DashboardRoutingModule {}
