import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StaffComponent } from './staff.component';
import { ScientificWorkComponent } from './scientific-work/scientific-work.component';
import { StaffDetailComponent } from "./staff-detail/staff-detail.component";

const routes: Routes = [
  {
    path: '',
    component: StaffComponent,
    pathMatch: 'full',
    children: [
      {
        path: ':staffId',
        component: StaffDetailComponent
      }
    ],
  },
  {
    path: 'scientific-works',
    component: ScientificWorkComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StaffRoutingModule {}
