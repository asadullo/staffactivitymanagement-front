import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StaffRoutingModule } from './staff-routing.module';
import { StaffComponent } from './staff.component';
import { ScientificWorkComponent } from './scientific-work/scientific-work.component';
import { MaterialModule } from "../../material/material.module";
import { StaffDetailComponent } from './staff-detail/staff-detail.component';
import {StaffService} from "./staff.service";

const COMPONENTS = [StaffComponent, ScientificWorkComponent];
@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, StaffRoutingModule, MaterialModule],
  declarations: [...COMPONENTS, StaffDetailComponent],
  providers: [StaffService],
})
export class StaffModule {}
