import { Component, OnInit } from '@angular/core';
import {IStaff} from "../../../definitions";
import {ActivatedRoute} from "@angular/router";
import {StaffService} from "../staff.service";

@Component({
  selector: 'ngx-staff-detail',
  templateUrl: './staff-detail.component.html',
  styleUrls: ['./staff-detail.component.scss']
})
export class StaffDetailComponent implements OnInit {

  staffId: string;
  staff: IStaff;
  constructor(private route: ActivatedRoute, private staffService: StaffService) {
    this.route.params.subscribe(params => {
      this.staffId = params['staffId'];
    })
    this.staff = staffService.findStaffById(this.staffId);
  }

  ngOnInit(): void {
    console.log('This staff: ', this.staff);
  }

}
