import { Component, OnInit } from '@angular/core';
import {IStaff} from "../../definitions";
import {StaffService} from "./staff.service";
import {Router} from "@angular/router";


@Component({
  selector: 'ngx-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.scss']
})
export class StaffComponent implements OnInit {
  staffs: IStaff[] = [];

  constructor(private readonly staffService: StaffService, private router: Router) {
    this.staffs = staffService.getStaffs();
  }

  ngOnInit(): void {
  }

  searchInput(event) {

  }

  selectStaff(id: string) {
    console.log('Keldim: ', id);
    this.router.navigate([`/pages/staff/${id}`] )
  }
}
