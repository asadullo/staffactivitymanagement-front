import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MiscellaneousRoutingModule } from './miscellaneous-routing.module';
import { MiscellaneousComponent } from './miscellaneous.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ThemeModule } from '../../@theme/theme.module';
import { NbButtonModule, NbCardModule } from '@nebular/theme';

@NgModule({
  declarations: [MiscellaneousComponent, NotFoundComponent],
  imports: [ThemeModule, NbCardModule, NbButtonModule, CommonModule, MiscellaneousRoutingModule],
})
export class MiscellaneousModule {}
