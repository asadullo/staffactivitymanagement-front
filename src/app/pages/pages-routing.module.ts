import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: 'layout',
        loadChildren: () => import('./layout/layout.module').then((m) => m.LayoutModule),
      },
      {
        path: 'staff',
        loadChildren: () => import('./staff/staff.module').then((m) => m.StaffModule),
      },

      {
        path: 'faculty',
        loadChildren: () => import('./faculty/faculty.module').then((m) => m.FacultyModule)
      },
      // {
      //   path: 'miscellaneous',
      //   loadChildren: () => import('./miscellaneous/miscellaneous.module').then((m) => m.MiscellaneousModule),
      // },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
      // {
      //   path: '**',
      //   redirectTo: 'miscellaneous/404',
      // },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
