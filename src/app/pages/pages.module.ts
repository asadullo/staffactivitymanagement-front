import { NgModule } from '@angular/core';
import { NbIconLibraries, NbMenuModule } from '@nebular/theme';

import {TranslateModule, TranslateService} from '@ngx-translate/core';
import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { PagesRoutingModule } from './pages-routing.module';

@NgModule({
  imports: [PagesRoutingModule, ThemeModule, NbMenuModule, DashboardModule, TranslateModule],
  declarations: [PagesComponent],
})
export class PagesModule {
  constructor(iconsLibrary: NbIconLibraries) {
    iconsLibrary.registerFontPack('fas', { packClass: 'fas', iconClassPrefix: 'fa' });
    iconsLibrary.registerFontPack('far', { packClass: 'far', iconClassPrefix: 'fa' });
    iconsLibrary.registerFontPack('fab', { packClass: 'fab', iconClassPrefix: 'fa' });
  }
}
